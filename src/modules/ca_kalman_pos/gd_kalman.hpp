#ifndef GD_KALMAN_HPP
#define GD_KALMAN_HPP

/**
 * @file gd_kalman.hpp
 *
 * kalman filter estimation code
 * @author Geetesh Dubey
 */

#pragma once

//#define MATRIX_ASSERT
//#define VECTOR_ASSERT

#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>
#include <fcntl.h>
#include <string.h>
#include <nuttx/config.h>
#include <nuttx/sched.h>
#include <sys/prctl.h>
#include <termios.h>
#include <math.h>
#include <mathlib/mathlib.h>
#include <uORB/uORB.h>
#include <uORB/topics/parameter_update.h>
#include <uORB/topics/actuator_controls.h>
#include <uORB/topics/actuator_armed.h>
#include <uORB/topics/sensor_combined.h>
#include <uORB/topics/vehicle_attitude.h>
#include <uORB/topics/vehicle_local_position.h>
#include <uORB/topics/vehicle_global_position.h>
#include <uORB/topics/vehicle_gps_position.h>
#include <uORB/topics/state_estimator_debug.h>
#include <uORB/topics/optical_flow.h>
#include <uORB/topics/ca_fovis_state_msg.h>
#include <drivers/drv_range_finder.h>
#include <uORB/topics/ca_global_state.h>
#include <mavlink/mavlink_log.h>
#include <poll.h>
#include <systemlib/err.h>
#include <geo/geo.h>
#include <systemlib/systemlib.h>
#include <drivers/drv_hrt.h>
#include <deque>


#include <systemlib/param/param.h>



class gd_kalman
{
public:
    /**
     * Constructor
     */
    gd_kalman();

    /**
     * Deconstuctor
     */

    virtual ~gd_kalman() {};

    //	math::Quaternion init(float ax, float ay, float az, float mx, float my, float mz);
    //Referenced from Probabilistic Robotics by Thrun
    //A = State Transition Matrix
    //R = Process noise
    //sig = State Covariance
    //Q = Observation Noise
    //C = Measurement prediction matrix
    //u = State [x; y; z; xv; yv; zv; xa; ya; za] all in NED
    //z = Measurement [xv_v; yv_v; zv_v; xv_f; yv_f; z_f; z_l; xa_i; ya_i; z_ai], viz Visual Odo , Optical Flow+Sonar , Laser Altimeter , Accelerometer
    math::Matrix<9,9> A,R,sig;
    math::Matrix<13,13> Q;
    math::Matrix<13,9> C;
    math::Vector<9> u;
    math::Vector<13> z;
    math::Matrix<3,3> rotMat;


    void main();
    //KALMAN predict and correct functions
    void predict(float dt);
    void correct(float dt);

    struct position_estimator_inav_params {
        float w_z_baro;
        float w_z_gps_p;
        float w_z_acc;
        float w_z_sonar;
        float w_xy_gps_p;
        float w_xy_gps_v;
        float w_xy_acc;
        float w_xy_flow;
        float w_xy_vo;
        float w_xy_g;
        float w_gps_flow;
        float w_acc_bias;
        float flow_k;
        float flow_q_min;
        float sonar_filt;
        float sonar_err;
        float land_t;
        float land_disp;
        float land_thr;
        int use_flow;
        int use_vo;
        int use_g;
        int use_acc;
        float corr_v_th;
        float sens_tf;
        float q_vo_good;
        float q_vo_bad;
        float q_flow_good;
        float q_flow_bad;
        float q_sonar_good;
        float q_sonar_bad;
        float q_laser_good;
        float q_laser_bad;
        float q_acc_good;
        float q_acc_bad;
        float q_vo_z_good;
        float q_vo_z_bad;
        float q_g_good;
        float q_g_bad;
        float q_R_XY;
        float q_R_Z;
    } params;

    struct position_estimator_inav_param_handles {
        param_t w_z_baro;
        param_t w_z_gps_p;
        param_t w_z_acc;
        param_t w_z_sonar;
        param_t w_xy_gps_p;
        param_t w_xy_gps_v;
        param_t w_xy_acc;
        param_t w_xy_flow;
        param_t w_xy_vo;
        param_t w_xy_g;
        param_t w_gps_flow;
        param_t w_acc_bias;
        param_t flow_k;
        param_t flow_q_min;
        param_t sonar_filt;
        param_t sonar_err;
        param_t land_t;
        param_t land_disp;
        param_t land_thr;
        param_t use_flow;
        param_t use_vo;
        param_t use_g;
        param_t use_acc;
        param_t corr_v_th;
        param_t sens_tf;
        param_t q_vo_good;
        param_t q_vo_bad;
        param_t q_flow_good;
        param_t q_flow_bad;
        param_t q_sonar_good;
        param_t q_sonar_bad;
        param_t q_laser_good;
        param_t q_laser_bad;
        param_t q_acc_good;
        param_t q_acc_bad;
        param_t q_vo_z_good;
        param_t q_vo_z_bad;
        param_t q_g_good;
        param_t q_g_bad;
        param_t q_R_XY;
        param_t q_R_Z;
    } param_handles_;

    /**
     * Initialize all parameter handles and values
     *
     */
    int parameters_init();

    /**
     * Update all parameters
     *
     */
    int parameters_update();


    int mavlink_fd;

    void inertial_filter_predict(float dt, float x[3]);

    void inertial_filter_correct(float e, float dt, float x[3], int i, float w);

    void write_debug_log(const char *msg, float dt, float x_est[3], float y_est[3], float z_est[3], float corr_acc[3], float corr_gps[3][2], float w_xy_gps_p, float w_xy_gps_v);
    void write_debug_log2(const char *msg, float dt, math::Vector<9> data);

    bool mov_avg(float meas, float* res);
    void updateRot(struct vehicle_attitude_s* att);

    const hrt_abstime gps_topic_timeout = 1000000;		// GPS topic timeout = 1s
    const hrt_abstime flow_topic_timeout = 1000000;	// optical flow topic timeout = 1s
    const hrt_abstime sonar_timeout = 150000;	// sonar timeout = 150ms
    const hrt_abstime sonar_valid_timeout = 1000000;	// estimate sonar distance during this time after sonar loss
    const hrt_abstime laser_valid_timeout = 1000000;	// estimate sonar distance during this time after sonar loss
    const hrt_abstime xy_src_timeout = 2000000;	// estimate position during this time after position sources loss
    const hrt_abstime global_src_timeout = 100000;
    const uint32_t updates_counter_len = 1000000;
    const uint32_t pub_interval = 10000;	// limit publish rate to 100 Hz
    const float max_flow = 1.0f;	// max flow value that can be used, rad/s


    bool thread_should_exit = false;     /**< Deamon exit flag */
    bool thread_running = false;     /**< Deamon status flag */
    bool verbose_mode = false;


    //*_G = Variance for the sensor when its GOOD
    //*_B = Variance for the sensor when its BAD
    float F_B = 1.0;
    float F_G = 0.001;//*15;
    float V_B = 1.0;
    float V_G = 0.002;//*15;
    float A_B = 1.0;
    float A_G = 0.01;
    float G_B = 1e7;
    float G_G = 0.01;

    std::deque<float> meas_vec;
    unsigned int avg_window_size;
    float prev_sum;
    float new_sum;
    float delta_yaw;
    bool global_pose_valid;
    bool laser_good;


    bool use_kf = true;

    unsigned int print_ctr = 0;
    unsigned int param_update_ctr = 0;

};




#endif // GD_KALMAN_HPP
