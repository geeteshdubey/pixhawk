/****************************************************************************
 *
 *   Copyright (c) 2012, 2013 PX4 Development Team. All rights reserved.
 *   Author: 	Geetesh Dubey
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name PX4 nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ****************************************************************************/

/**
 * @file ca_kalman_pos.cpp
 * kalman position estimator.
 *
 * @author Geetesh Dubey
 */



#include "gd_kalman.hpp"

#define MIN_VALID_W 0.00001f

static bool thread_should_exit = false;     /**< Deamon exit flag */
static bool thread_running = false;     /**< Deamon status flag */
static int ca_kalman_pos_task;             /**< Handle of deamon task / thread */
static bool verbose_mode = false;

 gd_kalman* KF;



/**
 * Deamon management function.
 */
extern "C" __EXPORT int ca_kalman_pos_main(int argc, char *argv[]);

/**
 * Mainloop of deamon.
 */
int kalman_demo_thread_main(int argc, char *argv[]);

/**
 * Print the correct usage.
 */
static void usage(const char *reason);

static void
usage(const char *reason)
{
    if (reason)
        fprintf(stderr, "%s\n", reason);

    warnx("usage: ca_kalman_pos {start|stop|status} [-p <additional params>]");
    exit(1);
}

/**
 * The deamon app only briefly exists to start
 * the background job. The stack size assigned in the
 * Makefile does only apply to this management task.
 *
 * The actual stack size should be set in the call
 * to task_create().
 */
int ca_kalman_pos_main(int argc, char *argv[])
{

    if (argc < 1) {
        usage("missing command");
    }

    if (!strcmp(argv[1], "start")) {
        if (thread_running) {
            warnx("already running");
            /* this is not an error */
            exit(0);
        }

        verbose_mode = false;


        if (argc > 1)
            if (!strcmp(argv[2], "-v")) {
                verbose_mode = true;
            }

        thread_should_exit = false;
        ca_kalman_pos_task = task_spawn_cmd("ca_kalman_pos",
                           SCHED_RR, SCHED_PRIORITY_MAX - 5, 8000,
                           kalman_demo_thread_main,
                           (argv) ? (char * const *) &argv[2] : (char * const *) NULL);
        thread_running = KF->thread_running;
        exit(0);
    }

    if (!strcmp(argv[1], "stop")) {
        if (thread_running) {
            KF->thread_should_exit = true;
            warnx("stop");
            thread_should_exit = true;

        } else {
            warnx("app not started");
        }

        exit(0);
    }

    if (!strcmp(argv[1], "status")) {
        if (thread_running) {
            warnx("app is running");

        } else {
            warnx("app not started");
        }

        exit(0);
    }

    usage("unrecognized command");
    exit(1);
}




int kalman_demo_thread_main(int argc, char *argv[])
{
    warnx("started");

   KF  = new gd_kalman;

   KF->main();


    warnx("stopped");
//    mavlink_log_info(mavlink_fd, "[inav] stopped");
    thread_running = false;
    return 0;

}
