/* Copyright 2014 Sanjiban Choudhury, Geetesh Dubey
 * ca_trajectory_control.cpp
 *
 *  Created on: Apr 4, 2014
 *      Author: sanjiban, geetesh
 */
#include "ca_trajectory_control.h"

#define TILT_COS_MAX  0.7f
#define SIGMA     0.000001f

TrajectoryControl::TrajectoryControl()
: task_should_exit_(false),
  control_task_(-1),
  mavlink_fd_(-1),

  /* subscriptions */
  att_sub_(-1),
  att_sp_sub_(-1),
  control_mode_sub_(-1),
  params_sub_(-1),
  manual_sub_(-1),
  arming_sub_(-1),
  local_pos_sub_(-1),
  pos_sp_triplet_sub_(-1),
  ca_traj_sub_(-1),
  ca_command_sub_(-1),
  ca_global_state_sub_(-1),

  /* publications */
  att_sp_pub_(-1),
  local_pos_sp_pub_(-1),
  global_vel_sp_pub_(-1),
  replan_pub_(-1),
  tc_debug_pub_(-1),

  ref_alt_(0.0f),
  ref_timestamp_(0),

  allow_spatial_position_control_(true),
  allow_altitude_position_control_(true),
  allow_spatial_velocity_control_(true),
  allow_altitude_velocity_control_(true),
  allow_spatial_thrust_control_(true),
  allow_altitude_thrust_control_(true),
  allow_thrust_feedforward_(true),
  replan_now(false),

  reset_pos_sp_(true),
  reset_alt_sp_(true),
  reset_int_z_(true),
  reset_int_z_manual_(false),
  reset_int_xy_(true),
  reset_ref_traj_time_(false),
  new_traj_(false),
  async_mode(true),
  curr_traj_seg_(-1),
  psi_traj_(0),
  isLanding(false),
  isLaunching(false){

  memset(&att_, 0, sizeof(att_));
  memset(&att_sp_, 0, sizeof(att_sp_));
  memset(&manual_, 0, sizeof(manual_));
  memset(&control_mode_, 0, sizeof(control_mode_));
  memset(&arming_, 0, sizeof(arming_));
  memset(&local_pos_, 0, sizeof(local_pos_));
  memset(&pos_sp_triplet_, 0, sizeof(pos_sp_triplet_));
  memset(&local_pos_sp_, 0, sizeof(local_pos_sp_));
  memset(&global_vel_sp_, 0, sizeof(global_vel_sp_));
//  memset(&trajectory_, 0, sizeof(trajectory_));
  memset(&ref_pos_, 0, sizeof(ref_pos_));
  memset(&ca_traj_, 0, sizeof(ca_traj_));
  memset(&ca_command_, 0, sizeof(ca_command_));
  memset(&tc_debug_, 0, sizeof(tc_debug_));
  memset(&global_state, 0, sizeof(global_state));

  ca_traj_vec.clear();
  //ca_rotation_vec_.clear();
  //ca_translation_vec_.clear();

  /*
   * Set default trajectory;
   */
//  trajectory_.x.zero();
  //  trajectory_.x(3) = 0.4f;
  //  trajectory_.x(4) = -0.12f;
  //  trajectory_.x(5) = 0.0096f;

//  trajectory_.y.zero();
//  trajectory_.z.zero();
//  trajectory_.psi.zero();
//  trajectory_.t_start = 0;
//  trajectory_.t_end = 0;


  params_.pos_p.zero();
  params_.vel_p.zero();
  params_.vel_i.zero();
  params_.vel_d.zero();
  params_.vel_max.zero();
  params_.vel_ff.zero();
  params_.sp_offs_max.zero();
  curr_seg_id = 0;
  pos_.zero();
  pos_sp_.zero();
  vel_.zero();
  vel_sp_.zero();
  sp_move_rate_.zero();
  vel_prev_.zero();
  thrust_traj_.zero();

  thrust_int_.zero();

  params_handles_.thr_min   = param_find("CP_THR_MIN");
  params_handles_.thr_max   = param_find("CP_THR_MAX");
  params_handles_.z_p     = param_find("CP_Z_P");
  params_handles_.z_vel_p   = param_find("CP_Z_VEL_P");
  params_handles_.z_vel_i   = param_find("CP_Z_VEL_I");
  params_handles_.z_vel_d   = param_find("CP_Z_VEL_D");
  params_handles_.z_vel_max = param_find("CP_Z_VEL_MAX");
  params_handles_.z_ff    = param_find("CP_Z_FF");
  params_handles_.xy_p    = param_find("CP_XY_P");
  params_handles_.xy_vel_p  = param_find("CP_XY_VEL_P");
  params_handles_.xy_vel_i  = param_find("CP_XY_VEL_I");
  params_handles_.xy_vel_d  = param_find("CP_XY_VEL_D");
  params_handles_.xy_vel_max  = param_find("CP_XY_VEL_MAX");
  params_handles_.xy_ff   = param_find("CP_XY_FF");
  params_handles_.tilt_max  = param_find("CP_TILT_MAX");
  params_handles_.land_speed  = param_find("CP_LAND_SP");
  params_handles_.take_off_speed = param_find("CP_TKOFF_SP");
  params_handles_.land_tilt_max = param_find("CP_LAND_TILT");
  params_handles_.land_alt  = param_find("CP_LAND_ALT");
  params_handles_.take_off_alt  = param_find("CP_TKOFF_ALT");
  params_handles_.rc_scale_pitch  = param_find("RC_SCALE_PITCH");
  params_handles_.rc_scale_roll = param_find("RC_SCALE_ROLL");
  params_handles_.debug = param_find("CP_DEBUG");
  params_handles_.closed_loop_pos = param_find("CP_POS_CL");
  params_handles_.sync_mode = param_find("CP_SYNC_MD");
  params_handles_.thrustff_flag = param_find("CP_FF_FLAG");
  params_handles_.replan_period_sec = param_find("CP_REP_PER");
  params_handles_.replan_ahead_sec = param_find("CP_REP_AHD");
  params_handles_.use_preset_z_sp = param_find("CP_IS_ZSP");
  /* fetch initial parameter values */
  ParametersUpdate(true);

  start_time=18446744000000000000;
  control_counter=0;
  yaw_ref=0;
  for (int i=0;i<3;i++) 
    for (int j=0;j<3;j++) rot_ref[i][j]=0;

//  replan_period_sec=2.5;
//  replan_ahead_sec=2;
}

TrajectoryControl::~TrajectoryControl() {
  if (control_task_ != -1) {
    /* task wakes up every 100ms or so at the longest */
    task_should_exit_ = true;

    /* wait for a second for the task to quit at our request */
    unsigned i = 0;

    do {
      /* wait 20ms */
      usleep(20000);

      /* if we have given up, kill it */
      if (++i > 50) {
        task_delete(control_task_);
        break;
      }
    } while (control_task_ != -1);
  }

  traj_control::g_control = nullptr;
}


int TrajectoryControl::Start() {
  ASSERT(control_task_ == -1);

  /* start the task */
  control_task_ = task_spawn_cmd("ca_trajectory_control",
                                 SCHED_DEFAULT,
                                 SCHED_PRIORITY_MAX - 5,
                                 2048,
                                 (main_t)&TrajectoryControl::TaskMainTrampoline,
                                 nullptr);

  if (control_task_ < 0) {
    warn("task start failed");
    return -errno;
  }

  return OK;
}

void TrajectoryControl::TaskMainTrampoline(int argc, char *argv[]) {
  traj_control::g_control->TaskMain();
}

void TrajectoryControl::TaskMain() {
  warnx("started");
  mavlink_fd_ = open(MAVLINK_LOG_DEVICE, 0);
  mavlink_log_info(mavlink_fd_, "[tc] started");

  // Step 1. Set up subscribers
  att_sub_ = orb_subscribe(ORB_ID(vehicle_attitude));
  att_sp_sub_ = orb_subscribe(ORB_ID(vehicle_attitude_setpoint));
  control_mode_sub_ = orb_subscribe(ORB_ID(vehicle_control_mode));
  params_sub_ = orb_subscribe(ORB_ID(parameter_update));
  manual_sub_ = orb_subscribe(ORB_ID(manual_control_setpoint));
  arming_sub_ = orb_subscribe(ORB_ID(actuator_armed));
  local_pos_sub_ = orb_subscribe(ORB_ID(vehicle_local_position));
  pos_sp_triplet_sub_ = orb_subscribe(ORB_ID(position_setpoint_triplet));
  ca_traj_sub_ = orb_subscribe(ORB_ID(ca_trajectory_msg));
  ca_command_sub_ = orb_subscribe(ORB_ID(vehicle_command));
  //ca_global_state_sub_ = orb_subscribe(ORB_ID(ca_global_state_msg));


  // Step 2. Get parameters
  ParametersUpdate(true);

  // Step 3. Not armed
  arming_.armed = false;

  // Step 4. Get an initial update
  PollSubscriptions();

  // Step 5. Set default values for states
  thrust_int_.zero();
  bool was_armed = false;
  hrt_abstime t_prev = 0;

  // Step 6. Wake up source
  struct pollfd fds[1];
  fds[0].fd = local_pos_sub_;
  fds[0].events = POLLIN;

  // Step 7. Main loop
  while (!task_should_exit_) {

    // Step 7a. Get an update for topics and parameters

    // wait for up to 500ms for data
    int pret = poll(&fds[0], (sizeof(fds) / sizeof(fds[0])), 500);
    // timed out - periodic check for task_should_exit_
    if (pret == 0) {
      continue;
    }
    // this is undesirable but not much we can do
    if (pret < 0) {
      warn("poll error %d, %d", pret, errno);
      continue;
    }

    PollSubscriptions();
    ParametersUpdate(false);

    // Step 7b. Get time and differential
    hrt_abstime t = hrt_absolute_time();
    dt_ = t_prev != 0 ? (t - t_prev) * 0.000001f : 0.0f;
    t_prev = t;

    // Step 7c. Reset setpoints and integrals on arming
    if (control_mode_.flag_armed && !was_armed) {
      reset_pos_sp_ = true;
      reset_alt_sp_ = true;
      reset_int_z_ = true;
      reset_int_xy_ = true;
    }

    // Step 7d. Update arming and reference time
    was_armed = control_mode_.flag_armed;
    UpdateRef();

    if (AllowControl()) {
      // Step 7e: Control Loop
      // I. Update position and velocity of state
      pos_(0) = local_pos_.x;
      pos_(1) = local_pos_.y;
      pos_(2) = local_pos_.z;

      vel_(0) = local_pos_.vx;
      vel_(1) = local_pos_.vy;
      vel_(2) = local_pos_.vz;

      ResetRefTrajTime(t);

      //II. Update control reference
      if (AllowManualInput()) {
        // In non trajectory following mode
        //II A. Reset allow flags (implying that dont forcefully disable a mode)
        allow_spatial_position_control_ = true;
        allow_altitude_position_control_ = true;
        allow_spatial_velocity_control_ = true;
        allow_altitude_velocity_control_ = true;
        allow_spatial_thrust_control_ = true;
        allow_altitude_thrust_control_ = true;
        allow_thrust_feedforward_ = params_.thrustff_flag;
        //II B. Reset reference time for trajectory origin
        reset_ref_traj_time_ = true;
//        ref_traj_time_ = t;

//        isLanding = false; TODO //findd spot to reset them somewhere
//        isLaunching = false; TODO
        //II C. Update (if needed) manual setpoint and its rates and thrust ff
        GetManualSetPoint();
        ca_traj_vec.clear();   //TODO remove all traj after going to maunal input.
        //ca_rotation_vec_.clear();
        //ca_translation_vec_.clear();

      } else {
        // In trajectory following mode
        //II A. Set allow flags
        pos_sp_triplet_.current.valid = false;    // Ensure position triplet is invalid to not trigger any waypoint logic
        if(params_.closed_loop_pos == 1)
            allow_spatial_position_control_ = true;  //false
        else
            allow_spatial_position_control_ = false;
        allow_altitude_position_control_ = true;
        allow_spatial_velocity_control_ = true;
        allow_altitude_velocity_control_ = true;
        allow_spatial_thrust_control_ = true;
        allow_altitude_thrust_control_ = true;
        allow_thrust_feedforward_ = params_.thrustff_flag;

        //II B. Update setpoint, rates and thrust ff
        UpdateFromTrajectory(t);

        //II C. Do something with yaw ?
        // Override yaw  - DISABLED
//         att_sp_.yaw_body = psi_traj_;
//         // Local trajectory
//         math::Matrix<3, 3> R_local;
//         R_local.from_euler(0.0f, 0.0f, att_sp_.yaw_body);
//         thrust_traj_ = R_local * thrust_traj_;
//         sp_move_rate_ = R_local * sp_move_rate_;

        if(params_.debug > 3 )
        {
               if (control_counter%10==0)
               {
                     mavlink_log_info(mavlink_fd_, "sp_rate_:[%.2f,%.2f,%.2f]",sp_move_rate_(0),sp_move_rate_(1),sp_move_rate_(2));
                     mavlink_log_info(mavlink_fd_, "thrus:[%.2f,%.2f,%.2f]",thrust_traj_(0),thrust_traj_(1),thrust_traj_(2));
               }
        }

      }
      tc_debug_.sp_mov_rate_x=sp_move_rate_(0);
      tc_debug_.sp_mov_rate_y=sp_move_rate_(1);
      tc_debug_.sp_mov_rate_z=sp_move_rate_(2);
      /*if (att_sp_pub_ > 0) {
        orb_publish(ORB_ID(vehicle_attitude_setpoint), att_sp_pub_, &att_sp_);

      } else {
        att_sp_pub_ = orb_advertise(ORB_ID(vehicle_attitude_setpoint), &att_sp_);
      }*/
      //III. Publish set point
      if (!PublishLocalSetPoint()) {
        warnx("couldnt publish local setpoint");
      }


      //IV. Compute control input
      if (IsIdle()) {
        // idle state, don't run controller and set zero thrust
        if (!AttainIdleState()) {
          warnx("couldnt attain idle state");
        }
      } else {
        ComputeControlInput();
      }
      /* publish trajectory control debug */
      if (tc_debug_pub_ > 0) {
        orb_publish(ORB_ID(trajectory_control_debug), tc_debug_pub_, &tc_debug_);
      } else {
        tc_debug_pub_ = orb_advertise(ORB_ID(trajectory_control_debug), &tc_debug_);
        warnx("couldnt publish local setpoint");
      }
    } else {
      // controller disabled, reset setpoints
      reset_alt_sp_ = true;
      reset_pos_sp_ = true;
      reset_int_z_ = true;
      reset_int_xy_ = true;
    }

    //Step 7f. reset altitude controller integral (hovering throttle) to manual throttle after manual throttle control
    reset_int_z_manual_ = control_mode_.flag_armed && control_mode_.flag_control_manual_enabled && !control_mode_.flag_control_climb_rate_enabled;
  }

  // Step 8. Clean up and exit
  warnx("stopped");
  mavlink_log_info(mavlink_fd_, "[tc] stopped");

  control_task_ = -1;
  _exit(0);
}


void TrajectoryControl::ComputeControlInput() {
  // Step 1. Run position & altitude controllers, calculate velocity setpoint
  if (!TrajectoryControl::PositionFeedback(pos_sp_, sp_move_rate_, vel_sp_)) {
    warnx("position feedback had a problem");
    vel_sp_.zero();
  }

  // Step 2. Override position control based on conditions
  // Step 2a. Disable altitude position control
  if (!AllowAltitudePositionControl()) {
    reset_alt_sp_ = true;
    vel_sp_(2) = 0.0f;
  }
  // Step 2b. Disable spatial position control
  if (!AllowSpatialPositionControl()) {
    reset_pos_sp_ = true;
    vel_sp_(0) = 0.0f;
    vel_sp_(1) = 0.0f;
  }
  // Step 2c. Use constant descend rate when landing, ignore altitude setpoint
  if (IsLanding()) {
    reset_alt_sp_ = true;
    isLaunching = false;
    vel_sp_(2) = (double)params_.land_speed;
    sp_move_rate_(2) = vel_sp_(2);//params_.land_speed;

    double height = (double)pos_(2);
    if(height < (float)params_.land_alt )//check if we have reached the desired height TODO
        printf("Current Height Landing: %.2f \n",height);
    else
        isLanding = false;

  }

  //printf("LANDING SPEED: %.2f \n",params_.land_speed);
  if (isLaunching) //might want to create a function that checks this TODO
  {
      reset_alt_sp_ = true;
      isLanding = false;
      vel_sp_(2) = -(double)params_.take_off_speed;
      sp_move_rate_(2) = vel_sp_(2);//params_.land_speed;
      double height = (double)pos_(2);
      if(height > (float)params_.take_off_alt )//check if we have reached the desired height TODO
          printf("Current Height Launching: %.2f \n",height);
      else
          isLaunching = false;
  }

  // Step 3. Saturate position control
  if (LimitSpeed()) {
    SaturateVector(params_.vel_max,vel_sp_);
  }

  if (AllowSpatialVelocityControl() && !AllowSpatialPositionControl()) {
      vel_sp_(0) = sp_move_rate_(0);
      vel_sp_(1) = sp_move_rate_(1);
  }

  // Step 4. Log position control input
  if (!PublishGlobalVelSetPoint()) {
    warnx("couldnt publish global velocity setpoint");
  }

  // Step 5. Thrust computing
  if (AllowThrustComputing()) {
    // A. Reset integrators (if at all)
    if (AllowAltitudeThurstComputing()) {
      ResetAltitudeIntegrator();
    } else {
      reset_int_z_ = true;
    }
    if (AllowSpatialThurstComputing()) {
      ResetSpatialIntegrator();
    } else {
      reset_int_xy_ = true;
    }

    // B. Compute velocity feedback
    math::Vector<3> thrust_sp;
    if (!VelocityFeedback(vel_sp_, sp_move_rate_, thrust_sp)) {
      warnx("couldnt compute thrust");
    }

    // C. Override velocity control based on conditions
    if (!AllowSpatialVelocityControl()) {
      thrust_sp(0) = 0.0f;
      thrust_sp(1) = 0.0f;
    }
    if (!AllowAltitudeVelocityControl()) {
      thrust_sp(2) = 0.0f;
    }

    /*if (control_counter%10==0)
      if(params_.debug > 3 )
        mavlink_log_info(mavlink_fd_, "thrust_sp_raw:[%.2f,%.2f,%.2f]",thrust_sp(0),thrust_sp(1),thrust_sp(2));
      */
    
    // D. Add thrust feedforwards
    if (AllowFeedForwardThrust()) {
      if (fabsf( thrust_traj_(2) ) > 0.01f) {
        float frac = thrust_sp(2)/thrust_traj_(2);
        math::Vector<3> thrust_ff = thrust_traj_;
        thrust_ff *= frac;
        thrust_ff(2) = 0;
        thrust_sp += thrust_ff;

        tc_debug_.thrust_ff_x=thrust_ff(0);
        tc_debug_.thrust_ff_y=thrust_ff(1);
        tc_debug_.thrust_ff_z=thrust_ff(2);
      }
    }

    // E. Override thrust computation based on conditions
    if (!AllowSpatialThurstComputing()) {
      thrust_sp(0) = 0.0f;
      thrust_sp(1) = 0.0f;
    }
    if (!AllowAltitudeThurstComputing()) {
      thrust_sp(2) = 0.0f;
    }

    // F. Saturate thrust
    SaturateThrust(thrust_sp);
    tc_debug_.thrust_sp_x=thrust_sp(0);
    tc_debug_.thrust_sp_y=thrust_sp(1);
    tc_debug_.thrust_sp_z=thrust_sp(2);
    tc_debug_.thrust_int_x=thrust_int_(0);
    tc_debug_.thrust_int_y=thrust_int_(1);
    tc_debug_.thrust_int_z=thrust_int_(2);
    
    /*if (control_counter%10==0)
      if(params_.debug > 3 )
        mavlink_log_info(mavlink_fd_, "thrust_sp_raw:[%.2f,%.2f,%.2f]",thrust_sp(0),thrust_sp(1),thrust_sp(2));
    */
    // G. Compute attitudes
    if (AllowSpatialThurstComputing()) {
      math::Matrix<3, 3> R;
      R.identity();

      ComputeDesiredRotation(thrust_sp, R);

      // copy rotation matrix to attitude setpoint topic
      memcpy(&att_sp_.R_body[0], R.data, sizeof(att_sp_.R_body));
      att_sp_.R_valid = true;

      // calculate euler angles, for logging only, must not be used for control
      math::Vector<3> euler = R.to_euler();
      att_sp_.roll_body = euler(0);
      att_sp_.pitch_body = euler(1);
      // yaw already used to construct rot matrix, but actual rotation matrix can have different yaw near singularity
    }

    // F. Publish control inputs
    att_sp_.thrust = thrust_sp.length();
    att_sp_.timestamp = hrt_absolute_time();

    if (att_sp_pub_ > 0) {
      orb_publish(ORB_ID(vehicle_attitude_setpoint), att_sp_pub_, &att_sp_);

    } else {
      att_sp_pub_ = orb_advertise(ORB_ID(vehicle_attitude_setpoint), &att_sp_);
    }

  } else {
    // Reset z integrator if not computing thrust
    reset_int_z_ = true;
  }
}

bool TrajectoryControl::PositionFeedback(const math::Vector<3> &pose_sp, const math::Vector<3> &sp_move_rate, math::Vector<3> &vel_sp) {
  math::Vector<3> pos_err = pose_sp - pos_;
  vel_sp_ = pos_err.emult(params_.pos_p) + sp_move_rate.emult(params_.vel_ff);
  return true;
}

bool TrajectoryControl::VelocityFeedback(const math::Vector<3> &vel_sp, const math::Vector<3> &sp_move_rate, math::Vector<3> &control) {
  /* velocity error */
  math::Vector<3> vel_err = vel_sp - vel_;

  /* derivative of velocity error, not includes setpoint acceleration */
  math::Vector<3> vel_err_d = (sp_move_rate - vel_).emult(params_.pos_p) - (vel_ - vel_prev_) / dt_;
  vel_prev_ = vel_;

  /* thrust vector in NED frame */
  control = vel_err.emult(params_.vel_p) + vel_err_d.emult(params_.vel_d) + thrust_int_;

  return true;
}

void TrajectoryControl::SaturateVector(const math::Vector<3> &vec_max, math::Vector<3> &vec) {
  float vec_norm = vec.edivide(vec_max).length();

  if (vec_norm > 1.0f) {
    vec /= vec_norm;
  }
}

bool TrajectoryControl::PublishLocalSetPoint() {
  /* fill local position setpoint */
  local_pos_sp_.x = pos_sp_(0);
  local_pos_sp_.y = pos_sp_(1);
  local_pos_sp_.z = pos_sp_(2);
  local_pos_sp_.yaw = att_sp_.yaw_body;

  /*if(params_.debug > 1 )
  {
    if (control_counter%5==0){
          mavlink_log_info(mavlink_fd_, "pub sp:[%.2f,%.2f,%.2f]",pos_sp_(0),pos_sp_(1),pos_sp_(2));
    }
  }*/

  /* publish local position setpoint */
  if (local_pos_sp_pub_ > 0) {
    orb_publish(ORB_ID(vehicle_local_position_setpoint), local_pos_sp_pub_, &local_pos_sp_);
    return true;
  } else {
    local_pos_sp_pub_ = orb_advertise(ORB_ID(vehicle_local_position_setpoint), &local_pos_sp_);
    return false;
  }

}

bool TrajectoryControl::PublishGlobalVelSetPoint() {
  global_vel_sp_.vx = vel_sp_(0);
  global_vel_sp_.vy = vel_sp_(1);
  global_vel_sp_.vz = vel_sp_(2);

  /* publish velocity setpoint */
  if (global_vel_sp_pub_ > 0) {
    orb_publish(ORB_ID(vehicle_global_velocity_setpoint), global_vel_sp_pub_, &global_vel_sp_);

  } else {
    global_vel_sp_pub_ = orb_advertise(ORB_ID(vehicle_global_velocity_setpoint), &global_vel_sp_);
  }
  return true;
}

void TrajectoryControl::PublishReplan() {

    /* fill replan */
    replan_.replan_now = replan_now;
    replan_.replan_time_usec = replan_time_usec;
    replan_.replan_traj_id = replan_traj_id;

    /* publish local position setpoint */
    if (replan_pub_ > 0) {
        orb_publish(ORB_ID(ca_replan_msg), replan_pub_, &replan_);
    } else {
        replan_pub_ = orb_advertise(ORB_ID(ca_replan_msg), &replan_);
    }
}

// Meta actions
void TrajectoryControl::GetManualSetPoint() {
  sp_move_rate_.zero();
  thrust_traj_.zero();

  const float alt_ctl_dz = 0.2f;
  const float pos_ctl_dz = 0.05f;

  if (control_mode_.flag_control_altitude_enabled) {
    /* reset alt setpoint to current altitude if needed */
    ResetAltSp();

    /* move altitude setpoint with throttle stick */
    sp_move_rate_(2) = -ScaleControl(manual_.z - 0.5f, 0.5f, alt_ctl_dz);
  }


  if (control_mode_.flag_control_position_enabled) {
    /* reset position setpoint to current position if needed */
    ResetPosSP();

    /* move position setpoint with roll/pitch stick */
    //TODO
    sp_move_rate_(0) = ScaleControl(-manual_.x / params_.rc_scale_pitch, 1.0f, pos_ctl_dz);
    sp_move_rate_(1) = ScaleControl(manual_.y / params_.rc_scale_roll, 1.0f, pos_ctl_dz);

  }

  /* limit setpoint move rate */
  float sp_move_norm = sp_move_rate_.length();

  if (sp_move_norm > 1.0f) {
    sp_move_rate_ /= sp_move_norm;
  }

  /* scale to max speed and rotate around yaw */
  math::Matrix<3, 3> R_yaw_sp;
  R_yaw_sp.from_euler(0.0f, 0.0f, att_sp_.yaw_body);
  sp_move_rate_ = R_yaw_sp * sp_move_rate_.emult(params_.vel_max);


  /* move position setpoint */
  pos_sp_ += sp_move_rate_ * dt_;
  /* check if position setpoint is too far from actual position */
  math::Vector<3> pos_sp_offs;
  pos_sp_offs.zero();

  if (control_mode_.flag_control_position_enabled) {
    pos_sp_offs(0) = (pos_sp_(0) - pos_(0)) / params_.sp_offs_max(0);
    pos_sp_offs(1) = (pos_sp_(1) - pos_(1)) / params_.sp_offs_max(1);
  }

  if (control_mode_.flag_control_altitude_enabled) {
    pos_sp_offs(2) = (pos_sp_(2) - pos_(2)) / params_.sp_offs_max(2);
  }

  float pos_sp_offs_norm = pos_sp_offs.length();

  if (pos_sp_offs_norm > 1.0f) {
    pos_sp_offs /= pos_sp_offs_norm;
    pos_sp_ = pos_ + pos_sp_offs.emult(params_.sp_offs_max);
  }
  //if(control_counter%20==0) mavlink_log_info(mavlink_fd_, "offset %.2f, %.2f",pos_sp_(2),pos_sp_offs(2));
}

bool TrajectoryControl::AttainIdleState() {
  math::Matrix<3, 3> R;
  R.identity();
  memcpy(&att_sp_.R_body[0], R.data, sizeof(att_sp_.R_body));
  att_sp_.R_valid = true;

  att_sp_.roll_body = 0.0f;
  att_sp_.pitch_body = 0.0f;
  att_sp_.yaw_body = att_.yaw;
  att_sp_.thrust = 0.0f;

  att_sp_.timestamp = hrt_absolute_time();

  /* publish attitude setpoint */
  if (att_sp_pub_ > 0) {
    orb_publish(ORB_ID(vehicle_attitude_setpoint), att_sp_pub_, &att_sp_);

  } else {
    att_sp_pub_ = orb_advertise(ORB_ID(vehicle_attitude_setpoint), &att_sp_);
  }
  return true;
}

void TrajectoryControl::ResetAltitudeIntegrator() {
  if (reset_int_z_) {
    reset_int_z_ = false;
    float i = params_.thr_min;

    if (reset_int_z_manual_) {
      i = manual_.z;

      if (i < params_.thr_min) {
        i = params_.thr_min;

      } else if (i > params_.thr_max) {
        i = params_.thr_max;
      }
    }
    thrust_int_(2) = -i;
  }
}

void TrajectoryControl::ResetSpatialIntegrator() {
  if (reset_int_xy_) {
    reset_int_xy_ = false;
    thrust_int_(0) = 0.0f;
    thrust_int_(1) = 0.0f;
  }
}

void TrajectoryControl::SaturateThrust(math::Vector<3> &thrust_sp) {
  /* limit thrust vector and check for saturation */
  bool saturation_xy = false;
  bool saturation_z = false;

  /* limit min lift */
  float thr_min = params_.thr_min;

  if (!control_mode_.flag_control_velocity_enabled && thr_min < 0.0f) {
    /* don't allow downside thrust direction in manual attitude mode */
    thr_min = 0.0f;
  }

  float tilt_max = params_.tilt_max;

  /* adjust limits for landing mode */
  if (IsLanding()) {
    /* limit max tilt and min lift when landing */
    tilt_max = params_.land_tilt_max;

    if (thr_min < 0.0f) {
      thr_min = 0.0f;
    }
  }

  /* limit min lift */
  if (-thrust_sp(2) < thr_min) {
    thrust_sp(2) = -thr_min;
    saturation_z = true;
  }

  if (AllowSpatialThurstComputing()) {
    /* limit max tilt */
    if (thr_min >= 0.0f && tilt_max < M_PI / 2 - 0.05f) {
      /* absolute horizontal thrust */
      float thrust_sp_xy_len = math::Vector<2>(thrust_sp(0), thrust_sp(1)).length();

      if (thrust_sp_xy_len > 0.01f) {
        /* max horizontal thrust for given vertical thrust*/
        float thrust_xy_max = -thrust_sp(2) * tanf(tilt_max);

        if (thrust_sp_xy_len > thrust_xy_max) {
          float k = thrust_xy_max / thrust_sp_xy_len;
          thrust_sp(0) *= k;
          thrust_sp(1) *= k;
          saturation_xy = true;
        }
      }
    }

  } else {
    /* thrust compensation for altitude only control mode */
    float att_comp;
    if (PX4_R(att_.R,2,2) > TILT_COS_MAX) {
      att_comp = 1.0f / PX4_R(att_.R,2,2);

    } else if (PX4_R(att_.R,2,2) > 0.0f) {
      att_comp = ((1.0f / TILT_COS_MAX - 1.0f) / TILT_COS_MAX) * PX4_R(att_.R,2,2) + 1.0f;
      saturation_z = true;

    } else {
      att_comp = 1.0f;
      saturation_z = true;
    }

    thrust_sp(2) *= att_comp;
  }

  /* limit max thrust */
  float thrust_abs = thrust_sp.length();

  if (thrust_abs > params_.thr_max) {
    if (thrust_sp(2) < 0.0f) {
      if (-thrust_sp(2) > params_.thr_max) {
        /* thrust Z component is too large, limit it */
        thrust_sp(0) = 0.0f;
        thrust_sp(1) = 0.0f;
        thrust_sp(2) = -params_.thr_max;
        saturation_xy = true;
        saturation_z = true;

      } else {
        /* preserve thrust Z component and lower XY, keeping altitude is more important than position */
        float thrust_xy_max = sqrtf(params_.thr_max * params_.thr_max - thrust_sp(2) * thrust_sp(2));
        float thrust_xy_abs = math::Vector<2>(thrust_sp(0), thrust_sp(1)).length();
        float k = thrust_xy_max / thrust_xy_abs;
        thrust_sp(0) *= k;
        thrust_sp(1) *= k;
        saturation_xy = true;
      }

    } else {
      /* Z component is negative, going down, simply limit thrust vector */
      float k = params_.thr_max / thrust_abs;
      thrust_sp *= k;
      saturation_xy = true;
      saturation_z = true;
    }

    thrust_abs = params_.thr_max;
  }

  /* update integrals */
  math::Vector<3> vel_err = vel_sp_ - vel_;
  if (AllowSpatialThurstComputing() && !saturation_xy) {
    thrust_int_(0) += vel_err(0) * params_.vel_i(0) * dt_;
    thrust_int_(1) += vel_err(1) * params_.vel_i(1) * dt_;
  }

  if (AllowAltitudeThurstComputing() && !saturation_z) {
    thrust_int_(2) += vel_err(2) * params_.vel_i(2) * dt_;

    /* protection against flipping on ground when landing */
    if (thrust_int_(2) > 0.0f) {
      thrust_int_(2) = 0.0f;
    }
  }
}

void TrajectoryControl::ComputeDesiredRotation(math::Vector<3> &thrust_sp, math::Matrix<3, 3> &R) {

  float thrust_abs = thrust_sp.length();
  /* desired body_z axis = -normalize(thrust_vector) */
  math::Vector<3> body_x;
  math::Vector<3> body_y;
  math::Vector<3> body_z;

  if (thrust_abs > SIGMA) {
    body_z = -thrust_sp / thrust_abs;

  } else {
    /* no thrust, set Z axis to safe value */
    body_z.zero();
    body_z(2) = 1.0f;
  }

  /* vector of desired yaw direction in XY plane, rotated by PI/2 */
  math::Vector<3> y_C(-sinf(att_sp_.yaw_body), cosf(att_sp_.yaw_body), 0.0f);

  if (fabsf(body_z(2)) > SIGMA) {
    /* desired body_x axis, orthogonal to body_z */
    body_x = y_C % body_z;

    /* keep nose to front while inverted upside down */
    if (body_z(2) < 0.0f) {
      body_x = -body_x;
    }

    body_x.normalize();

  } else {
    /* desired thrust is in XY plane, set X downside to construct correct matrix,
     * but yaw component will not be used actually */
    body_x.zero();
    body_x(2) = 1.0f;
  }

  /* desired body_y axis */
  body_y = body_z % body_x;

  /* fill rotation matrix */
  for (int i = 0; i < 3; i++) {
    R(i, 0) = body_x(i);
    R(i, 1) = body_y(i);
    R(i, 2) = body_z(i);
  }
}


bool TrajectoryControl::IsIdle() {
  return !control_mode_.flag_control_manual_enabled && pos_sp_triplet_.current.valid && (pos_sp_triplet_.current.type == position_setpoint_s::SETPOINT_TYPE_IDLE);
}

bool TrajectoryControl::AllowControl() {
  return control_mode_.flag_control_altitude_enabled ||
      control_mode_.flag_control_position_enabled ||
      control_mode_.flag_control_climb_rate_enabled ||
      control_mode_.flag_control_velocity_enabled;
}

bool TrajectoryControl::AllowManualInput() {
  return control_mode_.flag_control_manual_enabled;
}

bool TrajectoryControl::AllowAltitudePositionControl() {
  return control_mode_.flag_control_altitude_enabled && allow_altitude_position_control_;
}

bool TrajectoryControl::AllowSpatialPositionControl() {
  return control_mode_.flag_control_position_enabled && allow_spatial_position_control_;
}

bool TrajectoryControl::AllowSpatialVelocityControl() {
  return allow_spatial_velocity_control_;
}

bool TrajectoryControl::AllowAltitudeVelocityControl() {
  return allow_altitude_velocity_control_;
}

bool TrajectoryControl::AllowAltitudeThurstComputing() {
  return control_mode_.flag_control_climb_rate_enabled && allow_altitude_thrust_control_;
}

bool TrajectoryControl::AllowSpatialThurstComputing() {
  return control_mode_.flag_control_velocity_enabled && allow_spatial_thrust_control_;
}

bool TrajectoryControl::AllowFeedForwardThrust() {
  return !control_mode_.flag_control_manual_enabled && allow_thrust_feedforward_;
}

bool TrajectoryControl::IsLanding() {
    //TODO
    return (!control_mode_.flag_control_manual_enabled && pos_sp_triplet_.current.valid && (pos_sp_triplet_.current.type == position_setpoint_s::SETPOINT_TYPE_LAND) || isLanding == true );
}

bool TrajectoryControl::LimitSpeed() {
  return !control_mode_.flag_control_manual_enabled;
}

bool TrajectoryControl::AllowThrustComputing() {
  return (AllowAltitudeThurstComputing() || AllowSpatialThurstComputing());
}


int TrajectoryControl::ParametersUpdate(bool force) {
  bool updated;
  struct parameter_update_s param_upd;

  orb_check(params_sub_, &updated);

  if (updated) {
    orb_copy(ORB_ID(parameter_update), params_sub_, &param_upd);
  }

  if (updated || force) {
    param_get(params_handles_.thr_min, &params_.thr_min);
    param_get(params_handles_.thr_max, &params_.thr_max);
    param_get(params_handles_.tilt_max, &params_.tilt_max);
    param_get(params_handles_.land_speed, &params_.land_speed);
    param_get(params_handles_.take_off_speed, &params_.take_off_speed);
    param_get(params_handles_.land_tilt_max, &params_.land_tilt_max);

    param_get(params_handles_.land_alt, &params_.land_alt);
    param_get(params_handles_.take_off_alt, &params_.take_off_alt);

    param_get(params_handles_.rc_scale_pitch, &params_.rc_scale_pitch);
    param_get(params_handles_.rc_scale_roll, &params_.rc_scale_roll);

    float v;
    param_get(params_handles_.xy_p, &v);
    params_.pos_p(0) = v;
    params_.pos_p(1) = v;
    param_get(params_handles_.z_p, &v);
    params_.pos_p(2) = v;
    param_get(params_handles_.xy_vel_p, &v);
    params_.vel_p(0) = v;
    params_.vel_p(1) = v;
    param_get(params_handles_.z_vel_p, &v);
    params_.vel_p(2) = v;
    param_get(params_handles_.xy_vel_i, &v);
    params_.vel_i(0) = v;
    params_.vel_i(1) = v;
    param_get(params_handles_.z_vel_i, &v);
    params_.vel_i(2) = v;
    param_get(params_handles_.xy_vel_d, &v);
    params_.vel_d(0) = v;
    params_.vel_d(1) = v;
    param_get(params_handles_.z_vel_d, &v);
    params_.vel_d(2) = v;
    param_get(params_handles_.xy_vel_max, &v);
    params_.vel_max(0) = v;
    params_.vel_max(1) = v;
    param_get(params_handles_.z_vel_max, &v);
    params_.vel_max(2) = v;
    param_get(params_handles_.xy_ff, &v);
    params_.vel_ff(0) = v;
    params_.vel_ff(1) = v;
    param_get(params_handles_.z_ff, &v);
    params_.vel_ff(2) = v;

    params_.sp_offs_max = params_.vel_max.edivide(params_.pos_p) * 2.0f;
    param_get(params_handles_.debug,&(params_.debug));
    param_get(params_handles_.closed_loop_pos,&(params_.closed_loop_pos));
    param_get(params_handles_.sync_mode,&(params_.sync_mode));
    param_get(params_handles_.thrustff_flag,&(params_.thrustff_flag));

    param_get(params_handles_.replan_period_sec,&(params_.replan_period_sec));
    param_get(params_handles_.replan_ahead_sec,&(params_.replan_ahead_sec));
    param_get(params_handles_.use_preset_z_sp,&(params_.use_preset_z_sp));
  }

  return OK;
}

void TrajectoryControl::PollSubscriptions() {
  bool updated;

  orb_check(att_sub_, &updated);

  if (updated) {
    orb_copy(ORB_ID(vehicle_attitude), att_sub_, &att_);
  }

  orb_check(att_sp_sub_, &updated);

  if (updated) {
    orb_copy(ORB_ID(vehicle_attitude_setpoint), att_sp_sub_, &att_sp_);
  }

  orb_check(control_mode_sub_, &updated);

  if (updated) {
    orb_copy(ORB_ID(vehicle_control_mode), control_mode_sub_, &control_mode_);
  }

  orb_check(manual_sub_, &updated);

  if (updated) {
    orb_copy(ORB_ID(manual_control_setpoint), manual_sub_, &manual_);
  }

  orb_check(arming_sub_, &updated);

  if (updated) {
    orb_copy(ORB_ID(actuator_armed), arming_sub_, &arming_);
    //    mavlink_log_info(mavlink_fd_, "[GD] ARMING MSG RECVD");
    //    printf("[GD] ARMING MSG RECVD");
  }

  orb_check(local_pos_sub_, &updated);

  if (updated) {
    orb_copy(ORB_ID(vehicle_local_position), local_pos_sub_, &local_pos_);
  }

  orb_check(ca_traj_sub_, &updated);

  if (updated) {
    orb_copy(ORB_ID(ca_trajectory_msg), ca_traj_sub_, &ca_traj_);

    if(params_.debug > 0)
        mavlink_log_info(mavlink_fd_,"[GD] Recvd New Traj:%d, SEG:%d", ca_traj_.traj_id,ca_traj_.seq_id);

    if(ca_traj_.seq_id == 0)
    {
        //IF FIRST SEGMENT AND WE HAVE NO SEGMENTS WE PUSH IMMEDIATELY
        if(ca_traj_vec.size() == 0)
        {
            ca_traj_vec.push_back(ca_traj_);
            curr_seg_id = 0;

            if(params_.debug > 0)
                mavlink_log_info(mavlink_fd_,"Push new traj: %d, SEG: %d", ca_traj_.traj_id,ca_traj_.seq_id);

           if(!params_.sync_mode) //planner without time sync
                reset_ref_traj_time_ = true;
            else //onboard planner with time sync
            {
                reset_ref_traj_time_ = false;
            }
            //printf("\nFIRST SEGMENT");
            if(params_.debug > 0)
                mavlink_log_info(mavlink_fd_, "\nFIRST SEGMENT");
        }
        else if(checkTraj(ca_traj_))//IF WE HAD TRAJECTORIES IN THE PAST, FIND BEST POS TO INSERT
        {
            std::deque<ca_traj_struct_s>::iterator it = ca_traj_vec.end()-1;
            if(it->time_stop_usec<ca_traj_.time_start_usec) {
                //When a new trajectory does not interrupt any old trajectory but the traj vector is not clear either, this loop comes into play
                //We want to clear the old trajectories to maintain continuity in time.
                ca_traj_vec.clear();
                ca_traj_vec.push_back(ca_traj_);
                curr_seg_id=0;
            }
            else
            {
                it = ca_traj_vec.begin();
                it += curr_seg_id; //it_r += curr_seg_id; it_t += curr_seg_id;
                for(; it <ca_traj_vec.end(); it++)//,it_r++,it_t++)
                {
                    if((it->time_start_usec <= ca_traj_.time_start_usec)&&(ca_traj_.time_start_usec < it->time_stop_usec)) //FOUND A PLACE TO INSERT
                    {
                           it->time_stop_usec = ca_traj_.time_start_usec;

                        if (it < ca_traj_vec.end()-1)   {
                            ca_traj_vec.erase(it+1,ca_traj_vec.end());     //delete [it+1,end)
                        }

                        //push back just behind it. then get out of loop.
                        if (it->time_start_usec ==ca_traj_.time_start_usec){   //TODO if smaller, move to next position, then push back. If exactly the same start time, just replace it. don't push after that.

                            ca_traj_vec.erase(it);   //TODO if smaller, move to next position, then push back. If exactly the same start time, just replace it. don't push after that.
                        }
                        ca_traj_vec.push_back(ca_traj_);  //same error as erase
                        mavlink_log_info(mavlink_fd_, "\nNew Vector Size = %d",ca_traj_vec.size());

                        break;
                    }
                }
            }
        }
    }
    else if(ca_traj_vec.size() != 0)
    {
        if(checkTraj(ca_traj_))
        {
            ca_traj_vec.push_back(ca_traj_);
            if(params_.debug > 0)
                mavlink_log_info(mavlink_fd_, "PUSEHD latter [TRAJ,SEG]: [%d,%d]",ca_traj_.traj_id,ca_traj_.seq_id);
        }
    }
  }

  orb_check(ca_command_sub_, &updated);

    if (updated) {
        orb_copy(ORB_ID(vehicle_command), ca_command_sub_, &ca_command_);
        if(ca_command_.command == VEHICLE_CMD_NAV_LAND)
        {
            //SET LANDING STUFF HERE
            //printf("[GD] Recvd LANDING COMMAND\n");
            isLanding = true;
        }
        if(ca_command_.command == VEHICLE_CMD_NAV_TAKEOFF)
        {
            //SET LANDING STUFF HERE
            //printf("[GD] Recvd LAUNCH COMMAND\n");
            isLaunching = true;
        }
    }

    /*orb_check(ca_global_state_sub_,&updated);
    if (updated){
        orb_copy(ORB_ID(ca_global_state_msg), ca_global_state_sub_, &global_state);
        math::Quaternion q;
        bool found_nan=false;
        for(int i=0;i<7;i++)
            if(!isfinite(global_state.pose[i]))
            {
                found_nan = true;
                warnx("NAN in global pose");
            }
        if(!found_nan){
            //taking global R inverse right here
            float yaw=global_state.pose[6];
            R_global.from_euler(0.0f, 0.0f, -yaw);
            t_global(0)=global_state.pose[0];
            t_global(1)=global_state.pose[1];
            t_global(2)=global_state.pose[2];
        }
    }*/

}

float TrajectoryControl::ScaleControl(float ctl, float end, float dz) {
  if (ctl > dz) {
    return (ctl - dz) / (end - dz);

  } else if (ctl < -dz) {
    return (ctl + dz) / (end - dz);

  } else {
    return 0.0f;
  }
}

void TrajectoryControl::UpdateRef() {
  if (local_pos_.ref_timestamp != ref_timestamp_) {
    ref_timestamp_ = local_pos_.ref_timestamp;
    // TODO mode position setpoint in assisted modes

    map_projection_init(&ref_pos_, local_pos_.ref_lat, local_pos_.ref_lon);
    ref_alt_ = local_pos_.ref_alt;
  }
}

void TrajectoryControl::ResetPosSP() {
  if (reset_pos_sp_) {
    reset_pos_sp_ = false;
    pos_sp_(0) = pos_(0);
    pos_sp_(1) = pos_(1);
    mavlink_log_info(mavlink_fd_, "[tc] reset pos sp: %.2f, %.2f", (double)pos_sp_(0), (double)pos_sp_(1));
  }
}

void TrajectoryControl::ResetAltSp() {
  if (reset_alt_sp_) {
    reset_alt_sp_ = false;
    if(params_.use_preset_z_sp != 1)
        pos_sp_(2) = pos_(2);
    else
        pos_sp_(2) = params_.take_off_alt;
    if(!(isLanding || isLaunching))
        mavlink_log_info(mavlink_fd_, "[tc] reset alt sp: %.2f", -(double)pos_sp_(2));
  }
}
void TrajectoryControl::ResetNoSegPosRef(){
    //Its called when you are not in auto mode
    //In auto mode, if you have a segment for current time, then its called again
  no_seg_pos_ref_=pos_;
  no_seg_yaw_ref_=att_.yaw;
}

void TrajectoryControl::ResetRefTrajTime(hrt_abstime t) {
  if (reset_ref_traj_time_) {
    reset_ref_traj_time_ = false;
    ref_traj_time_ = t;
    ResetNoSegPosRef();
    if(!params_.sync_mode)
    { pos_ref_ = pos_;
      yaw_ref  = att_.yaw;
        for (int i=0;i<3;i++) 
           for (int j=0;j<3;j++) 
           rot_ref[i][j]=PX4_R(att_.R,i,j);
    }  
  }
}

void TrajectoryControl::SetRefTrajTime(uint64_t t) {
    ref_traj_time_ = t;
    if(!params_.sync_mode)
    { pos_ref_ = pos_;
      yaw_ref  = att_.yaw;
        for (int i=0;i<3;i++) 
           for (int j=0;j<3;j++) 
           rot_ref[i][j]=PX4_R(att_.R,i,j);
    }  
}
/*
void TrajectoryControl::UpdateTransformForTrajectory(){

    math::Matrix<3, 3> R_local;
    R_local.from_euler(0.0f, 0.0f, att_.yaw);

    math::Vector<3>t_local;
    t_local(0)=local_pos_.x;
    t_local(1)=local_pos_.y;
    t_local(2)=local_pos_.z;

    math::Matrix<3, 3> temp_R;
    math::Vector<3>temp_t;
    temp_R=R_local*R_global;
    temp_t=t_local-R_transform*t_global;
    if(isfinite(temp_R(0,0)))R_transform=temp_R;
    if(isfinite(temp_t(0)&&isfinite(temp_t(1)&&isfinite(temp_t(2))))) t_transform=temp_t;

}

void TrajectoryControl::RotateTrajectory(){
    //ignoring z completely
    UpdateTransformForTrajectory();
    float temp=pos_sp_(2);
    //if(control_counter%10==0)mavlink_log_info(mavlink_fd_, "rotate with %.2f, %.2f, %.2f",t_transform(0),t_transform(1),R_transform(0,0));
    pos_sp_ = R_transform*pos_sp_+t_transform;
    pos_sp_(2)=temp;
    sp_move_rate_=R_transform*sp_move_rate_;
    thrust_traj_ = R_transform*thrust_traj_;

    //math::Vector<3> euler = R_transform.to_euler();
    //att_sp_.yaw_body=att_sp_.yaw_body+euler(2);
}
*/
void TrajectoryControl::UpdateFromTrajectory(hrt_abstime time_now) {
    hrt_abstime curr_time=time_now;
    if(!params_.sync_mode)
        curr_time = (curr_time - ref_traj_time_); // resets curr time to 0 when we goto auto mode
    else  curr_time=curr_time-start_time;    //minus by an offset, to make it more looks smaller. to minimize communication with odroid.

  control_counter++;
  bool segment_found = false;
  std::deque<ca_traj_struct_s>::iterator it = ca_traj_vec.begin();
  int i=0;
  for(;it<ca_traj_vec.end();i++,it++)// LOOP THROUGH ALL SEG AND POP ONES IN CURRENT TIME
  {
        //printf("\nLOOPING SEGS %d :%f < %f <%f",i,it->time_start_usec/1e6,curr_time/1e6,it->time_stop_usec/1e6);

        if((it->time_start_usec <= curr_time)&&(curr_time < it->time_stop_usec)) //THIS IS CURRENT SEGMENT
        {
            curr_seg_id = i;
            segment_found = true;

            if (it->seq_id == 0 && params_.sync_mode)
            {
                SetRefTrajTime(it->time_start_usec);
            }
            
            break;
        }
  }

  whether_replan = false ;
  uint32_t replan_t_usec = params_.replan_ahead_sec*1e6;
  replan_time_usec = curr_time + replan_t_usec;
  //TODO does it not need to be initialized to begin
  for(; it < ca_traj_vec.end(); it++)         // loop through all segs and find the prediction segments
  {
      if((it->time_start_usec <= (curr_time + replan_t_usec))&&((curr_time + replan_t_usec) < it->time_stop_usec))
      {
          whether_replan=true;       //if found replan position, start replan, otherwise wait wait until it stops.
          replan_now = false;
          replan_traj_id = it->traj_id;
          break;
      }
  }
  //NOW DELETE THE OLD SEGMENTS AND UPDATE CURR_SEG_ID
  if(segment_found)
  {
      ResetNoSegPosRef();
      it = ca_traj_vec.begin();
      /*std::deque<ca_traj_struct_s>::iterator it_=it;
      for(int t=0;t<curr_seg_id;t++)it_++;*/
      if(curr_seg_id >=1 && (it+curr_seg_id<ca_traj_vec.end()))
      {
          ca_traj_vec.erase(it,it+curr_seg_id); //TODO this resutls in hook error stl list

          curr_seg_id = 0;
      }
  }
  else // NO SEGMENTS FOUND, CLEAR WHATEVER WAS IN THE PAST + ASK TO REPLAN NOW
  {
      whether_replan=true; replan_now = true;
      if(params_.debug > 2) {if(control_counter%10==0)mavlink_log_info(mavlink_fd_,"can't find segments.");}
  }

  if ((time_now-prev_replan_time)/1e6>params_.replan_period_sec)
  {
      prev_replan_time=time_now;
      if (whether_replan)
         {   PublishReplan();

              /*if(params_.debug > 3)
              {
                   mavlink_log_info(mavlink_fd_,"pub replan time %.2f traj_id %f ",replan_time_usec/1e6,replan_traj_id);
              }*/
         }
  }
  //GENERATE
  if(ca_traj_vec.size()>0 )
  {
      if(params_.sync_mode) curr_time =(curr_time-ref_traj_time_);  //Havent we already subtracted ref time from curr time?
      double t_poly=curr_time/1e6;
      GenerateThrust(t_poly,segment_found);
  }
  else GenerateThrust(0.0,false);// JUST DO HOVER


}

void TrajectoryControl::GenerateThrust(float t, bool segment_found)  //TODO temporaly changed by Shichao
{
    // Set point move rate
    sp_move_rate_.zero();
    thrust_traj_ = math::Vector<3>(0, 0, -9.81);

    //GENERATE POSE/VEL FROM POLYNOMIAL
    /*std::vector<ca_traj_struct_s>::iterator it=ca_traj_vec.begin();
    for(int t=0;t<curr_seg_id;t++)++it;
    std::deque<ca_traj_struct_s>::iterator it=ca_traj_vec.begin();
    for(int t=0;t<curr_seg_id;t++)++it;
    curr_seg = *it;*/
    polynomial trajectory;
    if(segment_found)
    {
        curr_seg = ca_traj_vec[curr_seg_id];
        for (int i = 0; i < 7; i++)
        {
            trajectory.x(i) = (curr_seg.coefficients[i]);
            trajectory.y(i) = (curr_seg.coefficients[7+i]);
            trajectory.z(i) = (curr_seg.coefficients[14+i]);
            trajectory.psi(i) = (curr_seg.coefficients[21+i]);
        }
    }
    else
    {
        //printf("\nNO SEGMENT FOUND, RESETTING");
        trajectory.reset(); //TODO, Meanwhile do following
    }
   

    float d0[7] = {1.0f,t, t*t, t*t*t, t*t*t*t, t*t*t*t*t, t*t*t*t*t*t};
    float d1[7] = {0.0f, 1.0f, 2.0f*t, 3.0f*t*t, 4.0f*t*t*t, 5.0f*t*t*t*t, 6.0f*t*t*t*t*t};
    float d2[7] = {0.0f, 0.0f, 2.0f, 6.0f*t, 12.0f*t*t, 20.0f*t*t*t, 30.0f*t*t*t*t};

    math::Vector<7> diff_vector_0 = math::Vector<7>(d0);
    math::Vector<7> diff_vector_1 = math::Vector<7>(d1);
    math::Vector<7> diff_vector_2 = math::Vector<7>(d2);

    if(segment_found)
    {
        float pos_sp_veh[3] = { 0.0f, 0.0f, 0.0f};
        pos_sp_veh[0] = diff_vector_0 * trajectory.x;
        pos_sp_veh[1] = diff_vector_0 * trajectory.y;
//        pos_sp_veh[2] = diff_vector_0 * trajectory_.z;
        sp_move_rate_(0) = diff_vector_1 * trajectory.x;
        sp_move_rate_(1) = diff_vector_1 * trajectory.y;
//        sp_move_rate_(2) = diff_vector_1 * trajectory_.z;

        thrust_traj_(0) += diff_vector_2 * trajectory.x;
        thrust_traj_(1) += diff_vector_2 * trajectory.y;
//        thrust_traj_(2) += diff_vector_2 * trajectory_.z;
        float poly_yaw=diff_vector_0 * trajectory.psi;

        if(!params_.sync_mode)
        {
            float pos_sp_ned[2] = { 0.0f, 0.0f};
                for (int i = 0; i < 2; i++) {
                    for (int j = 0; j < 3; j++) {
                        pos_sp_ned[i] += rot_ref[i][j] * pos_sp_veh[j];  //TODO add rotation reference  att_.R[][]
                    }
                }
            pos_sp_(0) = pos_sp_ned[0]+ pos_ref_(0);
            pos_sp_(1) = pos_sp_ned[1]+ pos_ref_(1);
            psi_traj_ = poly_yaw + yaw_ref;
            att_sp_.yaw_body = psi_traj_;

            math::Matrix<3, 3> R_local;
            R_local.from_euler(0.0f, 0.0f, yaw_ref);
            thrust_traj_ = R_local * thrust_traj_;
            sp_move_rate_ = R_local * sp_move_rate_;
         }
         else
         {
            pos_sp_(0) = pos_sp_veh[0];
            pos_sp_(1) = pos_sp_veh[1];
            //pos_sp_(2) = pos_sp_veh[2];  //TODO just add z setpoint.
            psi_traj_ = poly_yaw;
            att_sp_.yaw_body = psi_traj_;
            //RotateTrajectory();

         }
    }
    else
    {
        // else pos_sp remains same as old 1.
        pos_sp_(0) = no_seg_pos_ref_(0);
        pos_sp_(1) = no_seg_pos_ref_(1);
        att_sp_.yaw_body = no_seg_yaw_ref_;
    }
    /*if (control_counter%5==0)
    {
      if(params_.debug > 4 )
      {
            mavlink_log_info(mavlink_fd_, "XYZ sp[%.2f,%.2f %.2f]", pos_sp_(0),pos_sp_(1),pos_sp_(2));
	  }
      if(params_.debug > 4 )
      {
        mavlink_log_info(mavlink_fd_, "thrust_poly:[%.2f,%.2f,%.2f]",diff_vector_2 * trajectory_.x,diff_vector_2 * trajectory_.y,diff_vector_2 * trajectory_.z);
        mavlink_log_info(mavlink_fd_, "thrus_traj:[%.2f,%.2f,%.2f]",thrust_traj_(0),thrust_traj_(1),thrust_traj_(2));
	  }
    } */

    if (control_counter==100000) control_counter=0;
}

bool TrajectoryControl::checkTraj(ca_traj_struct_s new_traj)
{
    bool valid = false;
    ca_traj_struct_s prev = ca_traj_vec.back();//= *it;//*/
    /*std::deque<ca_traj_struct_s>::iterator it_=ca_traj_vec.end();
    --it_;
    ca_traj_struct_s prev = *it_;//*/
    if(replan_now && (new_traj.seq_id == 0))
    {
        valid = true;
        mavlink_log_info(mavlink_fd_, "REPLAN now traj:[%d,%d], PREV:[%d,%d]",ca_traj_.traj_id,ca_traj_.seq_id,prev.traj_id,prev.seq_id);
    }
    else if(prev.seq_id == 4)
    {
        //    IF THIS IS LAST SEGMENT THEN TRAJ_ID SHOULD BE +1     //new_traj.traj_id != (prev.traj_id+1 // to allow skipping of whole trajectory but should chk other continuities then
        if((new_traj.traj_id == (prev.traj_id+1)) && (new_traj.seq_id == 0))//FIRST SEGMENT OF NEW TRAJ
            valid = true;
    }
    else if((new_traj.traj_id == prev.traj_id) && (new_traj.seq_id == (prev.seq_id+1)))//NEXT SEGMENT OF SAME TRAJ
        valid = true;

    if(params_.debug > 0 && !valid)
        mavlink_log_info(mavlink_fd_, "INVALID Traj:[%d,%d], PREV:[%d,%d]",ca_traj_.traj_id,ca_traj_.seq_id,prev.traj_id,prev.seq_id);
    if(params_.debug > 0 && valid)
        mavlink_log_info(mavlink_fd_, "VALID Traj:[%d,%d]",ca_traj_.traj_id,ca_traj_.seq_id);
    //valid=true;
    return valid;
}
